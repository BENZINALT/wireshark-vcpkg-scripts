#!/bin/bash

REPO_DIR="$( dirname "$0" )/.."

SINGLE_PACKAGES=()

while read -r PKG_GLCI ; do
PKG_GLCI=${PKG_GLCI%/gitlab-ci.yml}
PKG_GLCI=${PKG_GLCI##*/}
    SINGLE_PACKAGES+=("${PKG_GLCI%/gitlab-ci.yml}")
done < <( grep -l "extends: *.build-vcpkg-single" "$REPO_DIR"/*/gitlab-ci.yml )

printf "%-15s %10s %10s\n" "Package" "Our ver" "vcpkg ver"

for PKG in "${SINGLE_PACKAGES[@]}" ; do
    OUR_VER=$(grep PKG_VERSION "$REPO_DIR/$PKG/gitlab-ci.yml" | awk '{gsub(/"/, "", $2); print $2}')
    OUR_VER=${OUR_VER%-*}

    VCPKG_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/master/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )
    VCPKG_VER=${VCPKG_VER%\"}
    VCPKG_VER=${VCPKG_VER#\"}

    APPEND=""
    if [ "$OUR_VER" != "$VCPKG_VER" ] ; then
        APPEND=" ⬅︎"
    fi

    printf "%-15s %10s %10s%s\n" "$PKG" "$OUR_VER" "$VCPKG_VER" "$APPEND"
done

# vcpkg-export
VE_COMMIT=$(awk '/PORT_VCPKG_COMMIT:/ {gsub(/"/, "", $2); print $2}' "$REPO_DIR/vcpkg-export/gitlab-ci.yml")

# XXX Add libiconv and zlib when we have a commit that includes vcpkg.json.
for PKG in gettext glib libxml2 ; do
    OUR_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/$VE_COMMIT/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )

    VCPKG_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/master/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )

    APPEND=""
    if [ "$OUR_VER" != "$VCPKG_VER" ] ; then
        APPEND=" ⬅︎"
    fi

    printf "%-15s %10s %10s%s\n" "$PKG" "$OUR_VER" "$VCPKG_VER" "$APPEND"
done

printf "\n"
printf "Repository versions\n"

PKG=brotli
REPO_VER=$(curl --silent https://api.github.com/repos/google/brotli/releases/latest | jq --raw-output .tag_name)
printf "%-15s %15s\n" "$PKG" "$REPO_VER"

PKG=c-ares
REPO_VER=$(curl --silent https://api.github.com/repos/c-ares/c-ares/releases/latest | jq --raw-output .tag_name)
printf "%-15s %15s\n" "$PKG" "$REPO_VER"

PKG=libmaxminddb
REPO_VER=$(curl --silent https://api.github.com/repos/maxmind/libmaxminddb/releases/latest | jq --raw-output .tag_name)
printf "%-15s %15s\n" "$PKG" "$REPO_VER"

PKG=lz4
REPO_VER=$(curl --silent https://api.github.com/repos/lz4/lz4/releases/latest | jq --raw-output .tag_name)
printf "%-15s %15s\n" "$PKG" "$REPO_VER"

PKG=nghttp2
REPO_VER=$(curl --silent https://api.github.com/repos/nghttp2/nghttp2/releases/latest | jq --raw-output .tag_name)
printf "%-15s %15s\n" "$PKG" "$REPO_VER"
