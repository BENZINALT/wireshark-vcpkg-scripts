# Build our own SpanDSP library.

spandsp:
  # It would be nice to be able to use a more official image, but Martin Storsjö's
  # repository is listed at https://www.mingw-w64.org/downloads/
  image: mstorsjo/llvm-mingw
  stage: build
  variables:
    PKG_VERSION: "0.0.6"
    WS_REVISION: "5"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "push"'
    when: manual
    allow_failure: true
  - if: '$CI_PIPELINE_SOURCE == "push"'
    changes:
      - spandsp
    when: always
  script:
    - build_prefix=$( pwd )
    - DEBIAN_FRONTEND=noninteractive apt-get update
    - DEBIAN_FRONTEND=noninteractive apt-get --yes install libtool libtool-bin libltdl-dev
    # soft-switch.org has an invalid certificate. Follow Homebrew's lead:
    - curl -JLO https://web.archive.org/web/20220329161120/https://www.soft-switch.org/downloads/spandsp/spandsp-${PKG_VERSION}.tar.gz
    - |
      sha256sum --check <<SHA256
      cc053ac67e8ac4bb992f258fd94f275a7872df959f6a87763965feabfdcc9465  spandsp-${PKG_VERSION}.tar.gz
      SHA256
    - |
      cat > remove-sources.grep <<FIN
        fax.c
        fax_modems.c
        image_translate.c
        t4_rx.c
        t4_tx.c
        t30.c
        t30_api.c
        t30_logging.c
        t31.c
        t35.c
        t38_core.c
        t38_gateway.c
        t38_non_ecm_buffer
        t38_terminal.c
      FIN
    - |
      set -x
      for arch in arm64 x64 ; do
        case $arch in
          arm64)
            host_triplet=aarch64-w64-mingw32
            ;;
          x64)
            host_triplet=x86_64-w64-mingw32
            ;;
        esac
        destdir=spandsp-${PKG_VERSION}-${WS_REVISION}-${arch}-windows-ws
        rm -rf "$destdir"*
        mkdir "$destdir"

        rm -rf spandsp-$PKG_VERSION
        tar -xf spandsp-$PKG_VERSION.tar.gz
        cd spandsp-$PKG_VERSION

        # https://stackoverflow.com/questions/67116092/ld-problem-rpl-malloc-cross-compiling-arm
        sed -i \
          -e 's/if test -n "$enable_tests"/if test "$enable_tests" = "yes"/' \
          -e 's/^AC_FUNC_MALLOC/#AC_FUNC_MALLOC/' \
          -e 's/^AC_FUNC_REALLOC/#AC_FUNC_REALLOC/' \
          -e 's/AC_CHECK_LIB(\[tiff\]/#AC_CHECK_LIB([tiff]/' \
          configure.ac
        for mf in src/Makefile.{am,in} ; do
          grep --invert --file=../remove-sources.grep $mf > $mf.tmp
          mv $mf.tmp $mf
        done
        sed -i -e '/^#define _SPANDSP_H_$/ a\
      #define __inline__ inline' src/spandsp.h.in
        sed -i -e '/^#include <tiffio.h>$/d' src/spandsp.h.in
        sed -i -e 's/^#if defined(SPANDSP_EXPOSE_INTERNAL_STRUCTURES)$/#if 0/' src/spandsp.h.in
        sed -i -e 's/band is \xa1\xd33dB./band is 3dB./' src/spandsp/noise.h
        sed -i -e 's/#if defined(_M_IX86)  ||  defined(_M_X64)/#if defined(_M_IX86)  ||  defined(_M_X64) || defined(_M_ARM64)/' src/spandsp/telephony.h
        sed -i -e 's/__inline__/inline/g' $( grep -rl __inline__ src | grep "\.h$" )

        ./autogen.sh
        ./configure \
          CC=/opt/llvm-mingw/bin/${host_triplet}-clang \
          CFLAGS="-DLIBSPANDSP_EXPORTS" \
          LD=/opt/llvm-mingw/bin/${host_triplet}-ld \
          --enable-shared --disable-static \
          --disable-doc \
          --disable-tests \
          --disable-test-data \
          --disable-builtin-tiff \
          --host=$host_triplet \
          --prefix=$build_prefix/$destdir
        make
        make install
        cd ..
        sed -i -e '/^#define _SPANDSP_H_$/ a\
      #define lfastrint lrint\
      #define lfastrintf lrintf\
      ' "$destdir/include/spandsp.h"
        sed -i -e '/^#include <spandsp.fast_convert.h>$/d' "$destdir/include/spandsp.h"
        mv "$destdir/lib/libspandsp.dll.a" "$destdir/lib/libspandsp-2.lib"
        {
          echo "Downloaded from https://www.soft-switch.org/downloads/spandsp/ and built using MinGW-w64"
          printf "\nOther comments:\n"
          echo "- LibTIFF support was stripped out"
          echo "- Fax support was stripped out (fax.c, fax_modems.c, image_translate.c, t4_rx.c, t4_tx.c, t30.c, t30_api.c, t30_logging.c, t31.c, t35.c, t38_core.c, t38_gateway.c, t38_non_ecm_buffer, t38_terminal.c)"
          echo "- lib/*.dll.a files were moved to bin/*.lib (including a version number based on the lib/*.dll file)"
        } | sed 's/$/\r/' > "$destdir/README.Wireshark"
        # Create zip, but without extra info such as timestamp and uid/gid (-X)
        zip -Xr "$destdir.zip" "$destdir"
      done
      set +x
    - sha256sum spandsp*.zip
  artifacts:
    paths:
      - ${PORT_NAME}-*.zip
  needs: []
